/***************************************************************************
 * SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
 * SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
 * SPDX-License-Identifier: GPL-3.0-or-later
 ***************************************************************************/
#ifndef SKGRECURRENTOPERATIONOBJECT_H
#define SKGRECURRENTOPERATIONOBJECT_H
/** @file
 * This file defines classes SKGRecurrentOperationObject.
 *
 * @author Stephane MANKOWSKI / Guillaume DE BURE
 */

#include "skgbankmodeler_export.h"
#include "skgobjectbase.h"
class SKGOperationObject;
class SKGDocumentBank;

/**
 * This class manages recurrent operation object
 */
class SKGBANKMODELER_EXPORT SKGRecurrentOperationObject final : public SKGObjectBase
{
public:
    /**
     * This enumerate defines the period unit
     */
    enum PeriodUnit {DAY = 0,   /**< day */
                     WEEK = 1, /**< week */
                     MONTH = 2, /**< month */
                     YEAR = 3   /**< year */
                    };
    /**
     * This enumerate defines the period unit
     */
    Q_ENUM(PeriodUnit)

    /**
     * Default constructor
     */
    explicit SKGRecurrentOperationObject();

    /**
     * Constructor
     * @param iDocument the document containing the object
     * @param iID the identifier in @p iTable of the object
     */
    explicit SKGRecurrentOperationObject(SKGDocument* iDocument, int iID = 0);

    /**
     * Copy constructor
     * @param iObject the object to copy
     */
    explicit SKGRecurrentOperationObject(const SKGObjectBase& iObject);

    /**
     * Copy constructor
     * @param iObject the object to copy
     */
    SKGRecurrentOperationObject(const SKGRecurrentOperationObject& iObject);

    /**
     * Operator affectation
     * @param iObject the object to copy
     */
    SKGRecurrentOperationObject& operator= (const SKGObjectBase& iObject);

    /**
     * Operator affectation
     * @param iObject the object to copy
     */
    SKGRecurrentOperationObject& operator= (const SKGRecurrentOperationObject& iObject);

    /**
     * Destructor
     */
    virtual ~SKGRecurrentOperationObject();

    /**
     * Get the parent operation
     * @param oOperation the parent operation
     * @return an object managing the error.
     *   @see SKGError
     */
    SKGError getParentOperation(SKGOperationObject& oOperation) const;

    /**
     * Set the parent operation
     * @param iOperation the parent operation
     * @return an object managing the error.
     *   @see SKGError
     */
    SKGError setParentOperation(const SKGOperationObject& iOperation);

    /**
     * Set the increment
     * @param iIncrement the number of @see setPeriodUnit
     * @return an object managing the error
     *   @see SKGError
     */
    SKGError setPeriodIncrement(int iIncrement);

    /**
     * Get the increment
     * @return the number
     */
    int getPeriodIncrement() const;

    /**
     * Get the period unit of this recurrent operation
     * @return the status
     */
    SKGRecurrentOperationObject::PeriodUnit getPeriodUnit() const;

    /**
     * Set the period unit of this recurrent operation
     * @param iPeriod the period unit
     * @return an object managing the error
     *   @see SKGError
     */
    SKGError setPeriodUnit(SKGRecurrentOperationObject::PeriodUnit iPeriod);

    /**
     * Set the number of days before term to create operation
     * @param iDays the number of days
     * @return an object managing the error
     *   @see SKGError
     */
    SKGError setAutoWriteDays(int iDays);

    /**
     * Get the number of days before term to create operation
     * @return the number of days
     */
    int getAutoWriteDays() const;

    /**
     * Set the number of days before term to warn user
     * @param iDays the number of days
     * @return an object managing the error
     *   @see SKGError
     */
    SKGError setWarnDays(int iDays);

    /**
     * Get the number of days before term to warn user
     * @return the number of days
     */
    int getWarnDays() const;

    /**
     * Set date of this recurrent operation
     * @param iDate the date
     * @return an object managing the error
     *   @see SKGError
     */
    // cppcheck-suppress passedByValue
    SKGError setDate(QDate iDate);

    /**
     * Get date of this recurrent operation
     * @return the date
     */
    QDate getDate() const;

    /**
     * Get next date of this recurrent operation
     * @return the date
     */
    QDate getNextDate() const;

    /**
    * Get all operations created by this recurrent operation
     * @param oOperations all operations
    * @return an object managing the error
    *   @see SKGError
    */
    SKGError getRecurredOperations(SKGListSKGObjectBase& oOperations) const;

    /**
     * To warn or not the end user
     * @param iWarn the warn: true or false
     * @return an object managing the error
     *   @see SKGError
     */
    SKGError warnEnabled(bool iWarn);

    /**
     * To know if the end user is warned or not
     * @return an object managing the error
     *   @see SKGError
     */
    bool isWarnEnabled() const;

    /**
     * To activate or not the auto write mode
     * @param iAutoWrite auto write mode: true or false
     * @return an object managing the error
     *   @see SKGError
     */
    SKGError autoWriteEnabled(bool iAutoWrite);

    /**
     * To know if auto write mode is enabled or not
     * @return an object managing the error
     *   @see SKGError
     */
    bool isAutoWriteEnabled() const;

    /**
     * To know if a time limit is enabled or not
     * @return an object managing the error
     *   @see SKGError
     */
    bool hasTimeLimit() const;

    /**
     * To enable / disable a time limit
     * @param iTimeLimit the time limit: true or false
     * @return an object managing the error
     *   @see SKGError
     */
    SKGError timeLimit(bool iTimeLimit);

    /**
     * Set the time limit
     * @param iTimeLimit the number of times operation will be inserted
     * @return an object managing the error
     *   @see SKGError
     */
    SKGError setTimeLimit(int iTimeLimit);

    /**
     * Set the time limit
     * @param iLastDate the last date of the operation will be inserted. setDate, setPeriodIncrement and setPeriodUnit must be used before.
     * @return an object managing the error
     *   @see SKGError
     */
    // cppcheck-suppress passedByValue
    SKGError setTimeLimit(QDate iLastDate);

    /**
     * Get the number of times operation will be inserted
     * @return the number of times
     */
    int getTimeLimit() const;

    /**
     * Warn and/or create operations for this recurrent operation
     * @param oNbInserted number of operations inserted
     * @param iForce to force the insertion even if autowrite is not enable
     * @param iDate date limit for insertion
     * @return an object managing the error
     *   @see SKGError
     */
    // cppcheck-suppress passedByValue
    SKGError process(int& oNbInserted, bool iForce = false, QDate iDate = QDate::currentDate());

    /**
     * Warn and/or create operations for all recurrent operations of the document
     * @param iDocument the document containing the object
     * @param oNbInserted number of operations inserted
     * @param iForce to force the insertion even if autowrite is not enable*
     * @param iDate date limit for insertion
     * @return an object managing the error
     *   @see SKGError
     */
    // cppcheck-suppress passedByValue
    static SKGError process(SKGDocumentBank* iDocument, int& oNbInserted, bool iForce = false, QDate iDate = QDate::currentDate());
};
/**
 * Declare the class
 */
Q_DECLARE_TYPEINFO(SKGRecurrentOperationObject, Q_MOVABLE_TYPE);
#endif
