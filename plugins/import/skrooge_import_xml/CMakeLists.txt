#***************************************************************************
#* SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE PLUGIN_IMPORT_XML ::..")

PROJECT(plugin_import_xml)

FIND_PACKAGE(Qt5 ${QT_MIN_VERSION} CONFIG REQUIRED XmlPatterns)

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

SET(skrooge_import_xml_SRCS
	skgimportpluginxml.cpp
)

KCOREADDONS_ADD_PLUGIN(skrooge_import_xml SOURCES ${skrooge_import_xml_SRCS} INSTALL_NAMESPACE "skrooge/import" JSON "metadata.json")
TARGET_LINK_LIBRARIES(skrooge_import_xml KF5::Parts Qt5::XmlPatterns skgbasemodeler skgbasegui skgbankmodeler skgbankgui)

########### install files ###############
INSTALL(FILES ISO20022.xslt DESTINATION ${KDE_INSTALL_DATADIR}/skrooge)
